import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';
import { StudentService } from './student-service';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImpl2Service extends StudentService {
  constructor() {
    super()
  }

  getStudents(): Observable<Student[]> {
    return of(this.students);
  };
  students: Student[] = [{
    id: 3,
    studentId: '602115002',
    name: 'Kullanan',
    surname: 'Thanachotanan',
    gpa: 2.57
  }];
}
